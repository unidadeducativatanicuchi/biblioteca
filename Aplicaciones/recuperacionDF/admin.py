from django.contrib import admin
from.models import Genero_literario
from.models import Libro
from.models import Autor
from.models import Profesion

# Register your models here.
admin.site.register(Genero_literario)
admin.site.register(Libro)
admin.site.register(Autor)
admin.site.register(Profesion)
