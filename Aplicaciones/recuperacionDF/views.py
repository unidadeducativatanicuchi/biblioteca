from django.shortcuts import render,redirect
from .models import Profesion, Genero_literario,Autor,Libro
from django.contrib import messages
# Create your views here.
def listadoProfesion(request):
    profesionBdd=Profesion.objects.all()
    return render(request,'profesion.html',{'profesiones':profesionBdd})


def guardarProfesion(request):
    nombre_df=request.POST["nombre_df"]
    descripcion_df=request.POST["descripcion_df"]

    #insertando datos mediante el ORM de Django
    nuevaProfesion=Profesion.objects.create(nombre_df=nombre_df,descripcion_df=descripcion_df)
    messages.success(request,'Profesion guardada exitosamente');
    return redirect('/')

def eliminarProfesion(request,idP_df):
    profesionEliminar=Profesion.objects.get(idP_df=idP_df)
    profesionEliminar.delete()
    messages.success(request,'Eliminado Exitosamente');
    return redirect('/')

def editarProfesion(request,idP_df):
    profesionEditar=Profesion.objects.get(idP_df=idP_df)
    return render(request,'editarProfesion.html' ,{'profesion': profesionEditar})
    path('',views.listadoProfesion),
    path('guardarProfesion/',views.guardarProfesion),
    path('eliminarProfesion/<idP_df>',views.eliminarProfesion),
    path('editarProfesion/<idP_df>',views.editarProfesion),
    path('procesarActualizacionProfesion/',views.procesarActualizacionProfesion),

def procesarActualizacionProfesion(request):
    idP_df=request.POST["idP_df"]
    nombre_df=request.POST["nombre_df"]
    descripcion_df=request.POST["descripcion_df"]


    #Insertando datos mediante el ORM de DJANGO
    profesionEditar=Profesion.objects.get(idP_df=idP_df)
    profesionEditar.nombre_df=nombre_df
    profesionEditar.descripcion_df=descripcion_df
    profesionEditar.save()
    messages.success(request,
      'Profesion ACTUALIZADA Exitosamente')
    return redirect('/')

    #generoliterio
def listadoGeneroL(request):
    generosBdd=Genero_literario.objects.all()
    return render(request,'generoL.html',{'generos':generosBdd})


def guardarGeneroL(request):
    nombreGl_df=request.POST["nombreGl_df"]
    descripcionGl_df=request.POST["descripcionGl_df"]
    foto_referenciaGl=request.FILES.get("foto_referenciaGl_df")

    #insertando datos mediante el ORM de Django
    nuevoGenero=Genero_literario.objects.create(nombreGl_df=nombreGl_df,descripcionGl_df=descripcionGl_df,foto_referenciaGl=foto_referenciaGl)
    messages.success(request,'Genero literario guardado exitosamente');
    return redirect('/literios/')

def eliminarGeneroL(request,idGl_df):
    generoEliminar=Genero_literario.objects.get(idGl_df=idGl_df)
    generoEliminar.delete()
    messages.success(request,'Eliminado Exitosamente');
    return redirect('/literarios/')

def editarGeneroL(request,idGl_df):
    generoEditar=Genero_literario.objects.get(idGl_df=idGl_df)
    return render(request,'editarGeneroL.html' ,{'genero': generoEditar})


def procesarActualizacionGeneroL(request):
    idGl_df=request.POST["idGl_df"]
    nombreGl_df=request.POST["nombreGl_df"]
    descripcionGl_df=request.POST["descripcionGl_df"]

    if 'foto_referenciaGl_df' in request.FILES:
            foto_referenciaGl = request.FILES.get("foto_referenciaGl_df")
    else:
            # Si no se proporciona una nueva imagen, conserva la existente
            generoL_existente = Genero_literario.objects.get(idGl_df=idGl_df)
            foto_referenciaGl_df = generoL_existente.foto_referenciaGl_df

    #Insertando datos mediante el ORM de DJANGO
    generoEditar=Genero_literario.objects.get(idGl_df=idGl_df)
    generoEditar.nombreGl_df=nombreGl_df
    generoEditar.descripcionGl_df=descripcionGl_df
    generoEditar.foto_referenciaGl_df=foto_referenciaGl_df
    generoEditar.save()
    messages.success(request,
      'Genero literario ACTUALIZADO Exitosamente')
    return redirect('/literarios/')

    #Autor
def listaAutor(request):
    autoresBdd=Autor.objects.all()
    profesionesBdd=Profesion.objects.all()
    return render(request,'autor.html',{'autores':autoresBdd, 'profesiones':profesionesBdd})


def guardarAutor(request):
    #Capturando los valores del formulario
    id_profesion=request.POST["id_profesion"]
    #capturando el tipo seleccionado por el usuario
    profesionSeleccionado=Profesion.objects.get(idP_df=id_profesion)
    apellido_df=request.POST["apellido_df"]
    nombre_df=request.POST["nombre_df"]
    edad_df=request.POST["edad_df"]
    hoja_vida_pdf=request.FILES.get("hoja_vida_pdf")

    #insertando datos mediante el ORM de Django
    nuevoAutor=Autor.objects.create(apellido_df=apellido_df,nombre_df=nombre_df,hoja_vida_pdf=hoja_vida_pdf,profesion=profesionSeleccionado)
    messages.success(request,'Autor guardado exitosamente');
    return redirect('/autores/')

def eliminarAutor(request,idA_df):
    autorEliminar=Autor.objects.get(idA_df=idA_df)
    autorEliminar.delete()
    messages.success(request,'Eliminado Exitosamente');
    return redirect('/autores/')

def editarAutor(request,idA_df):
    autorEditar=Autor.objects.get(idA_df=idA_df)
    profesionesBdd=Profesion.objects.all()
    return render(request,'editarAutor.html' ,{'autor': autorEditar,'profesiones':profesionesBdd})


def procesarActualizacionAutor(request):
    idA_df=request.POST["idA_df"]
    id_profesion=request.POST["id_profesion"]
    profesionSeleccionado=Profesion.objects.get(idP_df=idP_df)
    apellido_df=request.POST["apellido_df"]
    nombre_df=request.POST["nombre_df"]
    edad_df=request.POST["edad_df"]


    

    #Insertando datos mediante el ORM de DJANGO
    autorEditar=Autor.objects.get(idA_df=idA_df)
    autorEditar.profesion=profesionSeleccionado
    autorEditar.apellido_df=apellido_df
    autorEditar.nombre_df=nombre_df
    autorEditar.edad_df=edad_df
    autorEditar.hoja_vida_pdf=hoja_vida_pdf
    autorEditar.save()
    messages.success(request,
      'Autor ACTUALIZADO Exitosamente')
    return redirect('/autores/')

#Libro
def listaLibro(request):
    librosBdd=Libro.objects.all()
    generosBdd=Genero_literario.objects.all()
    autoresBdd=Autor.objects.all()
    return render(request,'libro.html',{'libros':librosBdd,'generos':generosBdd,'autores':autoresBdd})


def guardarLibro(request):
    #Capturando los valores del formulario
    id_genero=request.POST["id_genero"]
    id_autor=request.POST["id_autor"]
    #capturando el tipo seleccionado por el usuario
    generoSeleccionado=Genero_literario.objects.get(idGl_df=id_genero)
    autorSeleccionado=Autor.objects.get(idA_df=id_autor)
    titulo_df=request.POST["titulo_df"]
    editorial_df=request.POST["editorial_df"]
    fecha_publicacion_df=request.POST["fecha_publicacion_df"]
    imagen_portada_df=request.FILES.get("imagen_portada_df")

    #insertando datos mediante el ORM de Django
    nuevolibro=Libro.objects.create(titulo_df=titulo_df,editorial_df=editorial_df,fecha_publicacion_df=fecha_publicacion_df,imagen_portada_df=imagen_portada_df,generoliterario_df=generoSeleccionado,autor=autorSeleccionado)
    messages.success(request,'Libro guardado exitosamente');
    return redirect('/libros/')

def eliminarLibro(request,idL_df):
    libroEliminar=Libro.objects.get(idL_df=idL_df)
    libroEliminar.delete()
    messages.success(request,'Eliminado Exitosamente');
    return redirect('/libros/')

def editarLibro(request,idL_df):
    libroEditar=Libro.objects.get(idL_df=idL_df)
    generosBdd=Genero_literario.objects.all()
    autoresBdd=Autor.objects.all()
    return render(request,'editarLibro.html' ,{'libro': libroEditar,'generos':generosBdd,'autores':autoresBdd})


def procesarActualizacionLibro(request):
    idL_df=request.POST["idL_df"]
    id_genero=request.POST["id_genero"]
    id_autor=request.POST["id_autor"]
    generoSeleccionado=Genero_literario.objects.get(idGl_df=id_genero)
    autorSeleccionado=Autor.objects.get(idA_df=id_autor)
    titulo_df=request.POST["titulo_df"]
    editorial_df=request.POST["editorial_df"]
    fecha_publicacion_df=request.POST["fecha_publicacion_df"]

    if 'imagen_portada_df' in request.FILES:
            imagen_portada_df = request.FILES.get("imagen_portada_df")
    else:
            # Si no se proporciona una nueva imagen, conserva la existente
            libro_existente = Libro.objects.get(idL_df=idL_df)
            imagen_portada_df = libro_existente.imagen_portada_df

    #Insertando datos mediante el ORM de DJANGO
    libroEditar=Libro.objects.get(idL_df=idL_df)
    libroEditar.generoliterario_df=generoSeleccionado
    libroEditar.autor=autorSeleccionado
    libroEditar.titulo_df=titulo_df
    libroEditar.editorial_df=editorial_df
    libroEditar.fecha_publicacion_df=fecha_publicacion_df
    libroEditar.imagen_portada_df=imagen_portada_df
    libroEditar.save()
    messages.success(request,
      'Detalle ACTUALIZADO Exitosamente')
    return redirect('/libros/')