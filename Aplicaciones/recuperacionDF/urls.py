from django.urls import path
from . import views
urlpatterns=[

    path('',views.listadoProfesion),
    path('guardarProfesion/',views.guardarProfesion),
    path('eliminarProfesion/<idP_df>',views.eliminarProfesion),
    path('editarProfesion/<idP_df>',views.editarProfesion),
    path('procesarActualizacionProfesion/',views.procesarActualizacionProfesion),

    path('literarios/',views.listadoGeneroL, name='literarios'),
    path('guardarGeneroL/',views.guardarGeneroL),
    path('eliminarGeneroL/<idGl_df>',views.eliminarGeneroL),
    path('editarGeneroL/<idGl_df>',views.editarGeneroL),
    path('procesarActualizacionGeneroL/',views.procesarActualizacionGeneroL),

    path('autores/',views.listaAutor, name='autores'),
    path('guardarAutor/',views.guardarAutor),
    path('eliminarAutor/<idA_df>',views.eliminarAutor),
    path('editarAutor/<idA_df>',views.editarAutor),
    path('procesarActualizacionAutor/',views.procesarActualizacionAutor),

    path('libros/',views.listaLibro, name='libros'),
    path('guardarLibro/',views.guardarLibro),
    path('eliminarLibro/<idL_df>',views.eliminarLibro),
    path('editarLibro/<idL_df>',views.editarLibro),
    path('procesarActualizacionLibro/',views.procesarActualizacionLibro),
]
