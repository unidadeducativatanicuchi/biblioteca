from django.db import models

# Create your models here.
class Profesion(models.Model):
    idP_df=models.AutoField(primary_key=True)
    nombre_df=models.CharField(max_length=150)
    descripcion_df=models.TextField()

    def __str__(self):
        fila="{0}: {1} "
        return fila.format(self.nombre_df, self.descripcion_df)

class Genero_literario(models.Model):
    idGl_df=models.AutoField(primary_key=True)
    nombreGl_df=models.CharField(max_length=150)
    descripcionGl_df=models.TextField()
    foto_referenciaGl_df=models.FileField(upload_to='generoliterio', null=True,blank=True)

    def __str__(self):
        fila="{0}: {1} - {2}"
        return fila.format(self.nombreGl_df, self.descripcionGl_df, self.foto_referenciaGl_df)

class Autor(models.Model):
    idA_df=models.AutoField(primary_key=True)
    apellido_df=models.CharField(max_length=150)
    nombre_df=models.CharField(max_length=150)
    edad_df=models.CharField(max_length=150)
    hoja_vida_pdf=models.FileField(upload_to='hoja_vida_pdf', null=True, blank=True)
    profesion=models.ForeignKey(Profesion, null=True,blank=True,on_delete=models.CASCADE)

    def __str__(self):
        fila="{0}: {1} - {2} -{3}"
        return fila.format(self.apellido_df, self.nombre_df, self.edad_df, self.hoja_vida_pdf)



class Libro(models.Model):
    idL_df=models.AutoField(primary_key=True)
    titulo_df=models.CharField(max_length=150)
    editorial_df=models.CharField(max_length=150)
    fecha_publicacion_df=models.DateField()
    imagen_portada_df=models.FileField(upload_to='libro', null=True,blank=True)
    generoliterario_df=models.ForeignKey(Genero_literario, null=True,blank=True,on_delete=models.CASCADE)
    autor=models.ForeignKey(Autor, null=True,blank=True,on_delete=models.CASCADE)

    def __str__(self):
        fila="{0}: {1} - {2} -{3}"
        return fila.format(self.titulo_df, self.editorial_df, self.fecha_publicacion_df, self.imagen_portada_df)
